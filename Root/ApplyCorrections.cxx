#include <OfflineTimingTool/OfflineTimingTool.h>

//
// Apply offline timing corrections
//________________________________
double OfflineTimingTool::applyCorrections(caloObject_t &curr_obj){

  // Print the variables read from container
  if( m_debug ) dumpInfo( curr_obj );

  
  if( m_debug ) Info( "OfflineTimingTool::getCorrectedTime()", Form("Initial Time: t = %f, t_corr = %f",curr_obj.m_time, curr_obj.m_corrected_time));

  // Set valid flag
  if( Set_valid( curr_obj ) != EL::StatusCode::SUCCESS ){
    Error( "OfflineTimingTool::getCorrectedTime()", "Failed valid flag setting."); 
    curr_obj.valid = false;
  }
  
  // Return time = -99999 if the object is invalid
  if ( !curr_obj.valid ){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", "No corrections available. Returning valid = false, time = -99999.");
    curr_obj.m_corrected_time = -99999;
    return curr_obj.m_corrected_time;
  }

  // Apply time of flight correction
  if( tofCorrection( curr_obj ) != EL::StatusCode::SUCCESS ){
    Error( "OfflineTimingTool::getCorrectedTime()", "Failed TOF Correction. Returning valid = false, time = -99999.");  
    curr_obj.m_corrected_time = -99999;
    curr_obj.valid = false;
    return curr_obj.m_corrected_time;
  }
  
  if( m_debug ) Info( "OfflineTimingTool::getCorrectedTime()", Form("After TOF: t_corr = %f", curr_obj.m_corrected_time));

  // Apply avg FT time per run corr
  if( applyPass0Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg FT correction (pass0). Returning valid = false, time = -99999.");
    curr_obj.m_corrected_time = -99999;
    curr_obj.valid = false;
    return curr_obj.m_corrected_time;
  }
  
  if( m_debug ) Info( "OfflineTimingTool::getCorrectedTime()", Form("After FT: t_corr = %f", curr_obj.m_corrected_time));

  int feb  = getFebNo( curr_obj.m_onlId );
  unsigned int rN  = curr_obj.m_rn;
  if ( curr_obj.m_iov == 15 && feb == 73 && rN < 431341 ) {
    	if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", Form("Correcting FEB_EMBA2_06R_M2 from first few runs of IOV2 (Run %u)", rN));
	    curr_obj.m_corrected_time -= 1.768;
  }


  // Apply avg FEB time corr
  if( applyPass1Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg FEB correction. Returning valid = false, time = -99999.");  
    curr_obj.m_corrected_time = -99999;
    curr_obj.valid = false;
    return curr_obj.m_corrected_time;
  }

  if( m_debug ) Info( "OfflineTimingTool::getCorrectedTime()", Form("After FEB: t_corr = %f", curr_obj.m_corrected_time));
  
  // Apply avg ch time corr
  if( applyPass2Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg Ch correction (pass2). Returning valid = false, time = -99999.");  
    curr_obj.m_corrected_time = -99999;
    curr_obj.valid = false;
    return curr_obj.m_corrected_time;
  }

  if( m_debug ) Info( "OfflineTimingTool::getCorrectedTime()", Form("After Ch: t_corr = %f", curr_obj.m_corrected_time));

  // Apply avg energy time corr
  if( applyPass3Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg Energy Correction (pass3). Returning valid = false, time = -99999.");
    curr_obj.m_corrected_time = -99999;
    curr_obj.valid = false;
    return curr_obj.m_corrected_time;
  }

  if( m_debug ) Info( "OfflineTimingTool::getCorrectedTime()", Form("After Energy: t_corr = %f", curr_obj.m_corrected_time));

  // Apply angular cell position time corr
  if( applyPass4Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", "Failed dphi/deta Correction (pass4). Returning valid = false, time = -99999.");
    curr_obj.m_corrected_time = -99999;
    curr_obj.valid = false;
    return curr_obj.m_corrected_time;
  }

  if( m_debug ) Info( "OfflineTimingTool::getCorrectedTime()", Form("After dphi/deta: t_corr = %f", curr_obj.m_corrected_time));

  // Apply energy frac time corr
  if( applyPass5Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", "Failed f1/f3 Correction (pass4). Returning valid = false, time = -99999.");
    curr_obj.m_corrected_time = -99999;
    curr_obj.valid = false;
    return curr_obj.m_corrected_time;
  }

  if( m_debug )Info( "OfflineTimingTool::getCorrectedTime()", Form("After f1/f3: t_corr = %f", curr_obj.m_corrected_time));

  // Apply Avg Channel Time
  if( applyPass6Corr( curr_obj ) != EL::StatusCode::SUCCESS ){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", "Failed Avg Ch correction (pass6). Returning valid = false, time = -99999.");
    curr_obj.m_corrected_time = -99999;
    curr_obj.valid = false;
    return curr_obj.m_corrected_time;
  }

  if( m_debug ) Info( "OfflineTimingTool::getCorrectedTime()", Form("After Avg Ch correction (pass6): t_corr = %f", curr_obj.m_corrected_time));

  // Apply additional correction for photons
  if ( curr_obj.m_isPhoton ){
    if( applyPhotonCorr( curr_obj ) != EL::StatusCode::SUCCESS ){
      if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", "Failed additional photon correction. Returning valid = false, time = -99999.");
      curr_obj.m_corrected_time = -99999;
      curr_obj.valid = false;
      return curr_obj.m_corrected_time;
    }

    if( m_debug ) Info( "OfflineTimingTool::getCorrectedTime()", Form("After additional photon correction: t_corr = %f", curr_obj.m_corrected_time));
  }

  // Done with all passes

  return curr_obj.m_corrected_time;

}

//
// Apply Smearing to MC
//_______________________________________________________
double OfflineTimingTool::applySmearing(caloObject_t &calObj, double &t_coll, bool correlate, int mode){

  double t_raw = calObj.m_corrected_time; // TOF correction only  
  int gain     = calObj.m_gain;
  int slot     = getSlotNo( calObj.m_onlId ); 
  double en    = calObj.m_energy;

  if ( m_debug ) Info("OfflineTimingTool::applySmearing", Form("Pre-smeared time: %f, slot: %d, maxEcell_energy: %f, gain: %d, mode: %d",t_raw,slot,en,gain,mode));

  // Check that gain is medium or high
  if( gain != 0 && gain != 1){
    if ( m_debug ) Info( "OfflineTimingTool::applySmearing", Form("The corrected time is invalid, as this is a low gain event!"));
    calObj.valid = false;
    return calObj.m_corrected_time;
   }

  // Check slot number
  if( slot < 0 || slot == 8 || slot == 9 || slot > 21 ){
    if( m_debug ) Warning("OfflineTimingTool::applySmearing", Form("Unexpected slot. Smearing not available. Returning unsmeared time.") );
    calObj.valid = false;
    return calObj.m_corrected_time;
  }

  // Check that it's in EMB or EMEC 
  if(fabs(calObj.m_etas2) > 2.47 || (fabs(calObj.m_etas2) > 1.37 && fabs(calObj.m_etas2) < 1.52)){
    if ( m_debug) Info( "OfflineTimingTool::applySmearing", Form("The corrected time is invalid, as the object is not in EMB or EMEC!"));
    calObj.valid = false;
    return calObj.m_corrected_time;
  }

  // Check that mode is valid
  if ( mode != -1 && mode != 0 && mode!= 1 ){
    if ( m_debug ) Error( "OfflineTimingTool::applySmearing", Form("Invalid mode parameter for smearing (expected -1, 0, or 1). Returning unsmeared time."));
    calObj.valid = false;
    return calObj.m_corrected_time;
  }
  
  // Set smearing based on mode
  double sig_uncorr = 0.0;
  bool isEMB = slot >= 0 && slot <=7;
  int slotIndex = slot > 3 ? slot - 4 : slot;
  if (!isEMB) sig_uncorr = m_sig_uncorrEMEC;
  else if (gain == 0){
    if (en > 50.) sig_uncorr = smearUncorrFitH[3 * slotIndex + ((mode == -1) ? 2 : mode )]->Eval(50.);
    else          sig_uncorr = smearUncorrFitH[3 * slotIndex + ((mode == -1) ? 2 : mode )]->Eval(en);
  }
  else sig_uncorr = stod(smearUncorrFitM[slotIndex][1]) + ((double) mode) * stod(smearUncorrFitM[slotIndex][2]);

  if ( m_debug ) Info("OfflineTimingTool::applySmearing", Form("Correlated (beam) component sigma: %f, uncorrelated component sigma: %f",m_sig_beamSpread,sig_uncorr));

  // Seed random for smearing
  TRandom3 *m_random = new TRandom3();
  m_random->SetSeed((int) (1E6 * calObj.m_etas2)); // Use the same seed for the same object

  double t_random = m_random->Gaus(0, sig_uncorr);
  
  // For first electron, set collision time (second electron uses this value)
  if( !correlate ) t_coll = m_random->Gaus(0, m_sig_beamSpread);
   
  if ( m_debug) Info("OfflineTimingTool::applySmearing", Form("Collision time: %f",t_coll));

  //double t_random = m_random->Gaus(0, sig_uncorr);

  if ( m_debug) Info("OfflineTimingTool::applySmearing", Form("Random time: %f",t_random));

  double t_smear = t_raw + t_coll + t_random;

  delete m_random;

  if ( m_debug ) Info("OfflineTimingTool::applySmearing", Form("Smeared time: %f",t_smear));

  calObj.m_corrected_time = t_smear;

  return t_smear;

}


EL::StatusCode OfflineTimingTool::Set_valid(caloObject_t &calObj){

  // Check that gain is medium or high
  if(calObj.m_gain != 0 && calObj.m_gain != 1){
    calObj.valid = false;
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", Form("The corrected time is invalid, as this is a low gain event!"));
    return EL::StatusCode::SUCCESS; 
   } 
    
  // Check that it's in EMB or EMEC 
  if(fabs(calObj.m_etas2) > 2.47 || (fabs(calObj.m_etas2) > 1.37 && fabs(calObj.m_etas2) < 1.52)){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", Form("The corrected time is invalid, as the object is not in EMB or EMEC!"));
    calObj.valid = false;
    return EL::StatusCode::SUCCESS; 
  }

  // Check that the channel number is valid
  int ch   = getChannelNo( calObj.m_onlId );
  if( ch < 0 ){
    Warning( "OfflineTimingTool::getCorrectedTime()", Form("The corrected time is invalid, as the channel index < 0!"));
    calObj.valid = false;
    return EL::StatusCode::SUCCESS;
  }

  // Check that the IOV is valid
  int iov = calObj.m_iov;
  std::vector<int> bad_channelListTemp;
  if( iov < 3 ) 	bad_channelListTemp = bad_channelList15;
  else if ( iov < 8 ) 	bad_channelListTemp = bad_channelList16;
  else if ( iov < 11 )	bad_channelListTemp = bad_channelList17;
  else if ( iov < 14 )  bad_channelListTemp = bad_channelList18;
  else if ( iov < 16 )  bad_channelListTemp = bad_channelList22;
  else if ( iov < 30000 )  bad_channelListTemp = bad_channelList23;
  else{
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("The corrected time is invalid, as the IOV index is unexpected!!") );
    calObj.valid = false;
    return EL::StatusCode::SUCCESS;
  }

  // Check that maxEcell is not a bad channel, if so set time to -99999
  if(std::find(std::begin(bad_channelListTemp), std::end(bad_channelListTemp), ch) != std::end(bad_channelListTemp)){
    if ( m_debug) Info( "OfflineTimingTool::getCorrectedTime()", Form("The corrected time is invalid, as it's from a bad channel!"));
    calObj.valid = false;
    return EL::StatusCode::SUCCESS;
  }

  calObj.valid = true;  

  return EL::StatusCode::SUCCESS;
}
//
// Apply TOF correction
//________________________________________________________
EL::StatusCode OfflineTimingTool::tofCorrection(caloObject_t &calObj){
  
  // MaxEnergy Cell info
  double x = calObj.m_x; 
  double y = calObj.m_y;
  double z = calObj.m_z;
  double t = calObj.m_time;

  // Primary vertex z
  double PV_z = calObj.m_PV_z;
  
  // Speed of light in mm/ns
  const double c_mmns = 299.792458;
  
  // Distances for cell and Cell minus PV_z
  double r_cell = sqrt( x*x + y*y + z*z);
  double r_path = sqrt( x*x + y*y + (z - PV_z)*(z - PV_z));

  // TOF correction
  double t_tof = (r_path - r_cell)/c_mmns;

  // Save correction
  calObj.m_corrected_time =  t - t_tof;

  return EL::StatusCode::SUCCESS;

}

//
// Apply run by run avg FT corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass0Corr(caloObject_t &calObj){

  // Get FT number/gain
  int ft   = getFtNo( calObj.m_onlId );
  int gain = calObj.m_gain;
  int iov  = calObj.m_iov;
  unsigned int pos = calObj.m_rn_ind; // run index

  if ( ft < 0 ) return EL::StatusCode::FAILURE;
  if ( gain == 0  && ( ft >= (int)p0CorrH.at(iov).size() || 1+pos >= (unsigned int)p0CorrH.at(iov)[ft].size() ) ) return EL::StatusCode::FAILURE;
  if ( gain == 1  && ( ft >= (int)p0CorrM.at(iov).size() || 1+pos >= (unsigned int)p0CorrM.at(iov)[ft].size() ) ) return EL::StatusCode::FAILURE;

  // [ft][0] = ft index;  [ft][1-n] = run avg FT time
  double dt = (gain == 0 ? std::stod( p0CorrH.at(iov)[ft][1+pos] ) : 
                           std::stod( p0CorrM.at(iov)[ft][1+pos] ) );

  if ( dt == -99999 ) {
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 0 correction is invalid due to low statistics for gain %d FT index %d for run index %u",gain,ft,pos));
    return EL::StatusCode::FAILURE;
  }

  calObj.m_corrected_time -= dt; 

  return EL::StatusCode::SUCCESS;

}


//
// Apply avg FEB correcctions
//________________________________________________________
EL::StatusCode OfflineTimingTool::applyPass1Corr(caloObject_t &calObj){

  // Get FEB/gain
  int feb  = getFebNo( calObj.m_onlId );
  int gain = calObj.m_gain;
  int iov = calObj.m_iov;
  
  if( feb < 0 ) return EL::StatusCode::FAILURE;

  // Calculate time correction
  double dt = (gain == 0 ? std::stod( p1CorrH.at(iov)[feb] ) : 
                           std::stod( p1CorrM.at(iov)[feb] ) );

  if ( dt == -99999 ) {
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 1 correction is invalid due to low statistics for gain %d FEB index %d in IOV %d",gain,feb,iov));
    return EL::StatusCode::FAILURE;
  }

  calObj.m_corrected_time -= dt;

  return EL::StatusCode::SUCCESS;
}


//
// Apply the channel by channel avg corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass2Corr(caloObject_t &calObj){

  // Get ch/gain
  int ch   = getChannelNo( calObj.m_onlId );
  int gain = calObj.m_gain;
  int iov  = calObj.m_iov;

  if( ch < 0 ) return EL::StatusCode::FAILURE;

  // Calculate time correction
  double dt = (gain == 0 ? std::stod( p2CorrH.at(iov)[ch] ) :
                           std::stod( p2CorrM.at(iov)[ch] ) );

  if ( dt == -99999 ) {
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 2 correction is invalid due to low statistics for gain %d channel index %d in IOV %d",gain,ch,iov));
    return EL::StatusCode::FAILURE;
  }

  calObj.m_corrected_time -= dt; 

  return EL::StatusCode::SUCCESS;
}

//
// Apply the energy avg time corrections
//_________________________________________________________
EL::StatusCode OfflineTimingTool::applyPass3Corr(caloObject_t &calObj){

  // Get En/Slot/Gain
  double en = calObj.m_energy;
  int gain  = calObj.m_gain;
  int slot  = getSlotNo( calObj.m_onlId );
  int iov   = calObj.m_iov;

  if( slot < 0 ) return EL::StatusCode::FAILURE;

  // Vector to store points describing the smoothed fit
  std::vector< double > fitX, fitY;
  int nPoints = 0;
  switch (gain){
    case 0:
      nPoints = (int)p3CorrH.at(iov)[2 * slot].size() - 1;
      fitX.reserve(nPoints); fitY.reserve(nPoints);
      for (int i = 1; i < (int)p3CorrH.at(iov)[2 * slot].size(); i++){
        fitX.push_back(stod(p3CorrH.at(iov)[2 * slot][i]));
        fitY.push_back(stod(p3CorrH.at(iov)[2 * slot + 1][i]));
      } break;
    case 1:
      nPoints = (int)p3CorrM.at(iov)[2 * slot].size() - 1;
      fitX.reserve(nPoints); fitY.reserve(nPoints);
      for (int i = 1; i < (int)p3CorrM.at(iov)[2 * slot].size(); i++){
        fitX.push_back(stod(p3CorrM.at(iov)[2 * slot][i]));
        fitY.push_back(stod(p3CorrM.at(iov)[2 * slot + 1][i]));
      } break;
  }

  // Check that the fit is valid
  if ( fitX[0] == -99999 || fitY[0] == -99999 ){
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 3 correction is invalid due to low statistics for gain %d slot index %d in IOV %d",gain,slot,iov));
    return EL::StatusCode::FAILURE;
  }

  // Check that the energy is in the fit range
  if ( en < fitX[0] || en > fitX.back() ){
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 3 correction is invalid because the energy (%f GeV) is outside the range of energies with reliable corrections for gain %d slot index %d in IOV %d",en,gain,slot,iov));
    return EL::StatusCode::FAILURE;
  }

  TGraph *fit = new TGraph((int)fitX.size(),&fitX[0],&fitY[0]);

  double dt = fit->Eval(en);

  calObj.m_corrected_time -= dt; 

  delete fit;

  return EL::StatusCode::SUCCESS;
}

//
// Apply the dphi/deta corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass4Corr(caloObject_t &calObj){

  // Get dPhi/dEta/Slot/Gain
  double dphi = calObj.m_dphi;
  double deta = calObj.m_deta;
  int slot    = getSlotNo( calObj.m_onlId );
  int gain    = calObj.m_gain;
  int iov     = calObj.m_iov;

  if( slot < 0 ) return EL::StatusCode::FAILURE;

  std::vector< double > fitXPhi, fitYPhi, fitXEta, fitYEta;
  int nPointsPhi, nPointsEta = 0;
  switch (gain){
    case 0:
      nPointsPhi = (int)p4CorrH.at(iov)[4 * slot].size() - 1;
      fitXPhi.reserve(nPointsPhi); fitYPhi.reserve(nPointsPhi);
      for (int i = 1; i < (int)p4CorrH.at(iov)[4 * slot].size(); i++){
        fitXPhi.push_back(stod(p4CorrH.at(iov)[4 * slot][i]));
        fitYPhi.push_back(stod(p4CorrH.at(iov)[4 * slot + 1][i]));
      }
      nPointsEta = (int)p4CorrH.at(iov)[4 * slot + 2].size() - 1;
      fitXEta.reserve(nPointsEta); fitYEta.reserve(nPointsEta);
      for (int i = 1; i < (int)p4CorrH.at(iov)[4 * slot + 2].size(); i++){
        fitXEta.push_back(stod(p4CorrH.at(iov)[4 * slot + 2][i]));
        fitYEta.push_back(stod(p4CorrH.at(iov)[4 * slot + 3][i]));
      } break;
    case 1:
      nPointsPhi = (int)p4CorrM.at(iov)[4 * slot].size() - 1;
      fitXPhi.reserve(nPointsPhi); fitYPhi.reserve(nPointsPhi);
      for (int i = 1; i < (int)p4CorrM.at(iov)[4 * slot].size(); i++){
        fitXPhi.push_back(stod(p4CorrM.at(iov)[4 * slot][i]));
        fitYPhi.push_back(stod(p4CorrM.at(iov)[4 * slot + 1][i]));
      }
      nPointsEta = (int)p4CorrM.at(iov)[4 * slot + 2].size() - 1;
      fitXEta.reserve(nPointsEta); fitYEta.reserve(nPointsEta);
      for (int i = 1; i < (int)p4CorrM.at(iov)[4 * slot + 2].size(); i++){
        fitXEta.push_back(stod(p4CorrM.at(iov)[4 * slot + 2][i]));
        fitYEta.push_back(stod(p4CorrM.at(iov)[4 * slot + 3][i]));
      } break;
  }

  // Check that the fit is valid
  if ( fitXPhi[0] == -99999 || fitYPhi[0] == -99999 || fitXEta[0] == -99999 || fitYEta[0] == -99999 ){
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 4 correction is invalid due to low statistics for gain %d slot index %d in IOV %d",gain,slot,iov));
    return EL::StatusCode::FAILURE;
  }

  // Check that dphi and deta are in the fit ranges
  if ( dphi < fitXPhi[0] || dphi > fitXPhi.back() ){
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 4 correction is invalid because dphi (%f) is outside the range of dphi values with reliable corrections for gain %d slot index %d in IOV %d",dphi,gain,slot,iov));
    return EL::StatusCode::FAILURE;
  }
  if ( deta < fitXEta[0] || deta > fitXEta.back() ){
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 4 correction is invalid because deta (%f) is outside the range of deta values with reliable corrections for gain %d slot index %d in IOV %d",deta,gain,slot,iov));
    return EL::StatusCode::FAILURE;
  }

  TGraph *fitPhi = new TGraph((int)fitXPhi.size(),&fitXPhi[0],&fitYPhi[0]);
  TGraph *fitEta = new TGraph((int)fitXEta.size(),&fitXEta[0],&fitYEta[0]);

  // Correction
  double dtPhi = fitPhi->Eval(dphi);
  double dtEta = fitEta->Eval(deta);

  calObj.m_corrected_time -= (dtPhi + dtEta); 

  delete fitPhi;
  delete fitEta;

  return EL::StatusCode::SUCCESS;

}

//
// Apply the energy frac corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass5Corr(caloObject_t &calObj){

  // Get f1/f3/slot/gain
  double f1 = calObj.m_f1;
  double f3 = calObj.m_f3;
  int slot  = getSlotNo( calObj.m_onlId );
  int gain  = calObj.m_gain;
  int iov   = calObj.m_iov;

  if( slot < 0 ) return EL::StatusCode::FAILURE;

  std::vector< double > fitXF1, fitYF1, fitXF3, fitYF3;
  int nPoints1, nPoints3 = 0;
  switch (gain){
    case 0:
      nPoints1 = (int)p5CorrH.at(iov)[4 * slot].size() - 1;
      fitXF1.reserve(nPoints1); fitYF1.reserve(nPoints1);
      for (int i = 1; i < (int)p5CorrH.at(iov)[4 * slot].size(); i++){
        fitXF1.push_back(stod(p5CorrH.at(iov)[4 * slot][i]));
        fitYF1.push_back(stod(p5CorrH.at(iov)[4 * slot + 1][i]));
      }
      nPoints3 = (int)p5CorrH.at(iov)[4 * slot + 2].size() - 1;
      fitXF3.reserve(nPoints3); fitYF3.reserve(nPoints3);
      for (int i = 1; i < (int)p5CorrH.at(iov)[4 * slot + 2].size(); i++){
        fitXF3.push_back(stod(p5CorrH.at(iov)[4 * slot + 2][i]));
        fitYF3.push_back(stod(p5CorrH.at(iov)[4 * slot + 3][i]));
      } break;
    case 1:
      nPoints1 = (int)p5CorrM.at(iov)[4 * slot].size() - 1;
      fitXF1.reserve(nPoints1); fitYF1.reserve(nPoints1);
      for (int i = 1; i < (int)p5CorrM.at(iov)[4 * slot].size(); i++){
        fitXF1.push_back(stod(p5CorrM.at(iov)[4 * slot][i]));
        fitYF1.push_back(stod(p5CorrM.at(iov)[4 * slot + 1][i]));
      }
      nPoints3 = (int)p5CorrM.at(iov)[4 * slot + 2].size() - 1;
      fitXF3.reserve(nPoints3); fitYF3.reserve(nPoints3);
      for (int i = 1; i < (int)p5CorrM.at(iov)[4 * slot + 2].size(); i++){
        fitXF3.push_back(stod(p5CorrM.at(iov)[4 * slot + 2][i]));
        fitYF3.push_back(stod(p5CorrM.at(iov)[4 * slot + 3][i]));
      } break;
  }

  // Check that the fit is valid
  if ( fitXF1[0] == -99999 || fitYF1[0] == -99999 || fitXF3[0] == -99999 || fitYF3[0] == -99999 ){
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 5 correction is invalid due to low statistics for gain %d slot index %d in IOV %d",gain,slot,iov));
    return EL::StatusCode::FAILURE;
  }

  // Check that f1 and f3 are in the fit ranges
  if ( f1 < fitXF1[0] || f1 > fitXF1.back() ){
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 5 correction is invalid because f1 (%f) is outside the range of f1 values with reliable corrections for gain %d slot index %d in IOV %d",f1,gain,slot,iov));
    return EL::StatusCode::FAILURE;
  }
  if ( f3 < fitXF3[0] || f3 > fitXF3.back() ){
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 5 correction is invalid because f3 (%f) is outside the range of f3 values with reliable corrections for gain %d slot index %d in IOV %d",f3,gain,slot,iov));
    return EL::StatusCode::FAILURE;
  }

  TGraph *fitF1 = new TGraph((int)fitXF1.size(),&fitXF1[0],&fitYF1[0]);
  TGraph *fitF3 = new TGraph((int)fitXF3.size(),&fitXF3[0],&fitYF3[0]);

  // Correction
  double dtF1 = fitF1->Eval(f1);
  double dtF3 = fitF3->Eval(f3);

  calObj.m_corrected_time -= (dtF1 + dtF3);

  delete fitF1;
  delete fitF3;

  return EL::StatusCode::SUCCESS;

}

//
// Apply the channel by channel avg corrections
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPass6Corr(caloObject_t &calObj){

  // Get ch/gain
  int ch   = getChannelNo( calObj.m_onlId );
  int gain = calObj.m_gain;
  int iov  = calObj.m_iov;

  if( ch < 0 ) return EL::StatusCode::FAILURE;

  double dt = (gain == 0 ? std::stod( p6CorrH.at(iov)[ch] ) :
                           std::stod( p6CorrM.at(iov)[ch] ) );

  if ( dt == -99999 ) {
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Pass 6 correction is invalid due to low statistics for gain %d channel index %d in IOV %d",gain,ch,iov));
    return EL::StatusCode::FAILURE;
  }

  calObj.m_corrected_time -= dt; 

  return EL::StatusCode::SUCCESS;
}

//
// Apply the additional energy-based correction for photons
//_______________________________________________________
EL::StatusCode OfflineTimingTool::applyPhotonCorr(caloObject_t &calObj){

  double cellE = calObj.m_energy;
  double clusterE = calObj.m_clusterE;
  int gain  = calObj.m_gain;
  int slot  = getSlotNo( calObj.m_onlId );
  int iov = 1; // Corrections constant across IOVs

  if( slot < 0 ) return EL::StatusCode::FAILURE;

  // Correction only available for barrel photons
  if (slot > 7){
    if (m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Photon is not in the barrel, additional photon correction not available."));
    return EL::StatusCode::SUCCESS; //EL::StatusCode::FAILURE;
  }

  // Corrections symmetrized for A and C sides
  if (slot >= 4 && slot <= 7) slot -= 4;

  // Get correction
  if (cellE > 15.0){
    calObj.m_corrected_time += 0.025;
    return EL::StatusCode::SUCCESS;
  }

  // Vector to store points describing the smoothed fit
  std::vector< double > fitX, fitY;
  int nPoints = 0;
  switch (gain){
    case 0:
      nPoints = (int)pPhotonCorrH.at(iov)[2 * slot].size() - 1;
      fitX.reserve(nPoints); fitY.reserve(nPoints);
      for (int i = 1; i < (int)pPhotonCorrH.at(iov)[2 * slot].size(); i++){
        fitX.push_back(stod(pPhotonCorrH.at(iov)[2 * slot][i]));
        fitY.push_back(stod(pPhotonCorrH.at(iov)[2 * slot + 1][i]));
      } break;
    case 1:
      nPoints = (int)pPhotonCorrM.at(iov)[2 * slot].size() - 1;
      fitX.reserve(nPoints); fitY.reserve(nPoints);
      for (int i = 1; i < (int)pPhotonCorrM.at(iov)[2 * slot].size(); i++){
        fitX.push_back(stod(pPhotonCorrM.at(iov)[2 * slot][i]));
        fitY.push_back(stod(pPhotonCorrM.at(iov)[2 * slot + 1][i]));
      } break;
  }

  // Check that cluster energy is in the fit range
  if ( clusterE < fitX[0] || clusterE > fitX.back() ){
    if ( m_debug) Info("OfflineTimingTool::getCorrectedTime()", Form("Additional photon correction is invalid because the cluster energy (%f GeV) is outside the range of energies with reliable corrections for gain %d slot index %d",clusterE,gain,slot));
    return EL::StatusCode::FAILURE;
  }

  TGraph *fit = new TGraph((int)fitX.size(),&fitX[0],&fitY[0]);

  double dt = fit->Eval(clusterE);

  calObj.m_corrected_time -= dt;

  delete fit;

  return EL::StatusCode::SUCCESS;
}
