#include <OfflineTimingTool/OfflineTimingTool.h>

//
// Initialize the tool and load the corrections
//____________________________________________
OfflineTimingTool::OfflineTimingTool(bool debug){

  Info( "OfflineTimingTool::OfflineTimingTool()", "Initializing The Offline Precision Timing Tool" );
 
  if( debug ) Info( "OfflineTimingTool::OfflineTimingTool()", "Setting debug to true");
  m_debug = debug;

  if( initialize() != EL::StatusCode::SUCCESS ) Fatal( "OfflineTimingTool::OfflineTimingTool()", "Failed to properly initialize the tool!" );
  else Info( "OfflineTimingTool::OfflineTimingTool()", "Tool successfully initialized." );

}

//
// Load corrections to apply
//_________________________________________
EL::StatusCode OfflineTimingTool::initialize(){

  // Load resolution Parameters for MC smearing
  Info("OfflineTimingTool::initialize()", Form("  -> Loading parameters for MC Smearing"));

  //std::string inputName_SmearUncorrH = PathResolverFindCalibFile( "OfflineTimingTool/SmearUncorrH.dat");
  //std::ifstream fsmearh( inputName_SmearUncorrH );
  //std::string inputName_SmearUncorrM = PathResolverFindCalibFile( "OfflineTimingTool/SmearUncorrM.dat");
  //std::ifstream fsmearm( inputName_SmearUncorrM );

  std::ifstream fsmearh( PathResolverFindCalibFile( Form("OfflineTimingTool/SmearUncorrH.dat")) );
  std::ifstream fsmearm( PathResolverFindCalibFile( Form("OfflineTimingTool/SmearUncorrM.dat")) );

  // Make sure files opened correctly
  if( !fsmearh.is_open() ){
    Error("OfflineTimingTool::initialize()",Form("!!! Failed to open SmearUncorrH.dat"));
    return EL::StatusCode::FAILURE;
  }
  if( !fsmearm.is_open() ){
    Error("OfflineTimingTool::initialize()",Form("!!! Failed to open SmearUncorrM.dat"));
    return EL::StatusCode::FAILURE;
  }

  // Load parameters into vectors
  std::string l;
  std::vector< std::string > corr;
  std::vector< std::vector< std::string > > smearUncorrH;
  
  // Clear vector to avoid multiple initialization
  smearUncorrFitM.clear();

  while( std::getline(fsmearh, l) ){
    boost::split( corr, l, boost::is_any_of(" "), boost::token_compress_on);
    smearUncorrH.push_back( corr );
  }
  fsmearh.close();
  while( std::getline(fsmearm, l) ){
    boost::split( corr, l, boost::is_any_of(" "), boost::token_compress_on);
    smearUncorrFitM.push_back( corr );
  }
  fsmearm.close();
  if (m_debug) Info( "OfflineTimingTool::initialize()", Form(" ->Successfully read smearing parameters, %lu High and %lu Med Uncorrections",
         smearUncorrH.size(), smearUncorrFitM.size()) );

  // Create TGraphs
  for (int sl = 0; sl < 4; sl++){
    for (int var = 0; var < 3; var++){
      std::vector< double > fitX, fitY;
      int nPoints = (int)smearUncorrH[6 * sl + 2 * var].size() - 1;
      fitX.reserve(nPoints); fitY.reserve(nPoints);
      for (int i = 1; i < nPoints + 1; i++){
        fitX.push_back(stod(smearUncorrH[6 * sl + 2 * var][i]));
        fitY.push_back(stod(smearUncorrH[6 * sl + 2 * var + 1][i]));
      }
      smearUncorrFitH.push_back(new TGraph((int)fitX.size(),&fitX[0],&fitY[0]));
    }
  }

  Info("OfflineTimingTool::initialize()", Form("    -> Successfully loaded parameters for MC Smearing"));

  // Load configuration files
  Info( "OfflineTimingTool::initialize()", "  -> Loading correction configuration files");

  // Update this config file
  std::string inputName = PathResolverFindCalibFile( "OfflineTimingTool/RunNumberList.txt");
  std::ifstream f1( inputName );
  // Make sure file opened correctly
  if( !f1.is_open() ){
    Error("OfflineTimingTool::initialize()",Form(" !!! Failed to open RunNumber.txt "));
    return EL::StatusCode::FAILURE;
  }
  std::string inputName_bad_channel15 = PathResolverFindCalibFile( "OfflineTimingTool/bad_channel15.txt");
  std::ifstream f_bad_channel15( inputName_bad_channel15 );
  // Make sure file opened correctly
  if( !f_bad_channel15.is_open() ){
    Error("OfflineTimingTool::initialize()",Form("  !!! Failed to open bad_channel15.txt "));
    return EL::StatusCode::FAILURE;
  }
  std::string inputName_bad_channel16 = PathResolverFindCalibFile( "OfflineTimingTool/bad_channel16.txt");
  std::ifstream f_bad_channel16( inputName_bad_channel16 );
  // Make sure file opened correctly
  if( !f_bad_channel16.is_open() ){
    Error("OfflineTimingTool::initialize()",Form("  !!! Failed to open bad_channel16.txt "));
    return EL::StatusCode::FAILURE;
  }
  std::string inputName_bad_channel17 = PathResolverFindCalibFile( "OfflineTimingTool/bad_channel17.txt");
  std::ifstream f_bad_channel17( inputName_bad_channel17 );
  // Make sure file opened correctly
  if( !f_bad_channel17.is_open() ){
    Error("OfflineTimingTool::initialize()",Form("  !!! Failed to open bad_channel17.txt "));
    return EL::StatusCode::FAILURE;
  }
  std::string inputName_bad_channel18 = PathResolverFindCalibFile( "OfflineTimingTool/bad_channel18.txt");
  std::ifstream f_bad_channel18( inputName_bad_channel18 );
  // Make sure file opened correctly
  if( !f_bad_channel18.is_open() ){
    Error("OfflineTimingTool::initialize()",Form("  !!! Failed to open bad_channel18.txt "));
    return EL::StatusCode::FAILURE;
  }
  std::string inputName_bad_channel22 = PathResolverFindCalibFile( "OfflineTimingTool/bad_channel22.txt");
  std::ifstream f_bad_channel22( inputName_bad_channel22 );
  // Make sure file opened correctly
  if( !f_bad_channel22.is_open() ){
    Error("OfflineTimingTool::initialize()",Form("  !!! Failed to open bad_channel22.txt "));
    return EL::StatusCode::FAILURE;
  }
  std::string inputName_bad_channel23 = PathResolverFindCalibFile( "OfflineTimingTool/bad_channel23.txt");
  std::ifstream f_bad_channel23( inputName_bad_channel23 );
  // Make sure file opened correctly
  if( !f_bad_channel23.is_open() ){
    Error("OfflineTimingTool::initialize()",Form("  !!! Failed to open bad_channel23.txt "));
    return EL::StatusCode::FAILURE;
  }

  std::string inputName_iov = PathResolverFindCalibFile( "OfflineTimingTool/iov.txt");
  std::ifstream f_iov( inputName_iov );
  // Make sure file opened correctly
  if( !f_iov.is_open() ){
    Error("OfflineTimingTool::initialize()",Form("  !!! Failed to open iov.txt "));
    return EL::StatusCode::FAILURE;
  }

  // Clear vectors to avoid multiple initialization
  iovNumberList.clear(); 
  runNumberList.clear();
  bad_channelList15.clear();
  bad_channelList16.clear();
  bad_channelList17.clear();
  bad_channelList18.clear();
  p0CorrH.clear(); p0CorrM.clear();
  p1CorrH.clear(); p1CorrM.clear();
  p2CorrH.clear(); p2CorrM.clear();
  p3CorrH.clear(); p3CorrM.clear();
  p4CorrH.clear(); p4CorrM.clear();
  p5CorrH.clear(); p5CorrM.clear();
  p6CorrH.clear(); p6CorrM.clear();
  pPhotonCorrH.clear(); pPhotonCorrM.clear();

  int runNum;
  // Store each run into local vector
  while( f1 >> runNum ){
    runNumberList.push_back(runNum);
  }
  f1.close();

  int badchNum;
  // Store each bad channel into local vector
  while( f_bad_channel15 >> badchNum ){
    bad_channelList15.push_back(badchNum);
  }
  f_bad_channel15.close();
  // Store each bad channel into local vector
  while( f_bad_channel16 >> badchNum ){
    bad_channelList16.push_back(badchNum);
  }
  f_bad_channel16.close();
  // Store each bad channel into local vector
  while( f_bad_channel17 >> badchNum ){
    bad_channelList17.push_back(badchNum);
  }
  f_bad_channel17.close();
  // Store each bad channel into local vector
  while( f_bad_channel18 >> badchNum ){
    bad_channelList18.push_back(badchNum);
  }
  f_bad_channel18.close();

  int iovNum;
  // Store each iov size into local vector
  while( f_iov >> iovNum ){
    iovNumberList.push_back(iovNum);
  }
  f_iov.close();

  // Print some sample loaded run numbers, channels, IOVs, etc.
  if ( runNumberList.empty() ){
    Error( "OfflineTimingTool::initialize()", "runNumberList is empty. Run numbers were not loaded properly from RunNumberList.txt.");
    return EL::StatusCode::FAILURE;
  }
  else if ( m_debug ){
    Info( "OfflineTimingTool::initialize()", Form("Total Number of Runs: %lu", runNumberList.size()) );
    Info( "OfflineTimingTool::initialize()", Form("Run %d: %d", 0, runNumberList[0]) );
    Info( "OfflineTimingTool::initialize()", Form("Run %lu: %d", runNumberList.size()-1, runNumberList.back() ) );
  }
  if ( !bad_channelList15.empty() && m_debug ) {
    Info( "OfflineTimingTool::initialize()", Form("Total Number of bad channels for 2015: %lu", bad_channelList15.size()) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %d: %d", 0, bad_channelList15[0]) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %lu: %d", bad_channelList15.size()-1, bad_channelList15.back() ) );
  }
  if ( !bad_channelList16.empty() && m_debug ) {
    Info( "OfflineTimingTool::initialize()", Form("Total Number of bad channels for 2016: %lu", bad_channelList16.size()) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %d: %d", 0, bad_channelList16[0]) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %lu: %d", bad_channelList16.size()-1, bad_channelList16.back() ) );
  }
  if ( !bad_channelList17.empty()  && m_debug ) {
    Info( "OfflineTimingTool::initialize()", Form("Total Number of bad channels for 2017: %lu", bad_channelList17.size()) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %d: %d", 0, bad_channelList17[0]) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %lu: %d", bad_channelList17.size()-1, bad_channelList17.back() ) );
  }
  if ( !bad_channelList18.empty()  && m_debug ) {
    Info( "OfflineTimingTool::initialize()", Form("Total Number of bad channels for 2018: %lu", bad_channelList18.size()) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %d: %d", 0, bad_channelList18[0]) );
    Info( "OfflineTimingTool::initialize()", Form("Bad channel %lu: %d", bad_channelList18.size()-1, bad_channelList18.back() ) );
  }
  if ( iovNumberList.empty() ) {
    Error("OfflineTimingTool::initialize()", "!!! iovNumberList is empty. Number of runs per IOV not loaded properly from iov.txt.");
    return EL::StatusCode::FAILURE;
  }
  else if ( m_debug ) {
    Info( "OfflineTimingTool::initialize()", Form("Total Number of IOVs: %lu", iovNumberList.size()) );
    Info( "OfflineTimingTool::initialize()", Form("IOV %d: %d runs", 1, iovNumberList[0]) );
    Info( "OfflineTimingTool::initialize()", Form("IOV %lu: %d runs", iovNumberList.size()-1, iovNumberList.back() ) );
  }
 
  // Loop over IOVs
  for( size_t iov = 1; iov <= iovNumberList.size(); iov++){

    if ( m_debug ) Info("OfflineTimingTool::initialize()", Form("Loading Corrections for IOV%lu",iov));

    // Insert IOV key into maps
    p0CorrH.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );
    p0CorrM.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );
    p1CorrH.insert( std::pair<  int, std::vector< std::string> >
                             (iov, std::vector< std::string>()) );
    p1CorrM.insert( std::pair<  int, std::vector< std::string> >
                             (iov, std::vector< std::string>()) );
    p2CorrH.insert( std::pair<  int, std::vector< std::string> >
                             (iov, std::vector< std::string>()) );
    p2CorrM.insert( std::pair<  int, std::vector< std::string> >
                             (iov, std::vector< std::string>()) );
    p3CorrH.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );
    p3CorrM.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );
    p4CorrH.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );
    p4CorrM.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );
    p5CorrH.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );
    p5CorrM.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );
    p6CorrH.insert( std::pair<  int, std::vector< std::string> >
                             (iov, std::vector< std::string>()) );
    p6CorrM.insert( std::pair<  int, std::vector< std::string> >
                             (iov, std::vector< std::string>()) );
    pPhotonCorrH.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );
    pPhotonCorrM.insert( std::pair<  int, std::vector< std::vector<std::string> > >
                             (iov, std::vector< std::vector<std::string> >()) );

    // Correction Files to read
    std::vector <std::string> configHigh   {Form("OfflineTimingTool/IOV%lu/runByRunFTOffsetsH.dat",     iov), 
                                            Form("OfflineTimingTool/IOV%lu/AvgFebOffsetsH.dat",         iov),            
                                            Form("OfflineTimingTool/IOV%lu/AvgChOffsetsH.dat",          iov),                 
                                            Form("OfflineTimingTool/IOV%lu/AvgEnSlFitH.dat",            iov),             
                                            Form("OfflineTimingTool/IOV%lu/AvgdPhidEtaSlFitH.dat",      iov),             
                                            Form("OfflineTimingTool/IOV%lu/Avgf1f3SlFitH.dat",          iov),                  
                                            Form("OfflineTimingTool/IOV%lu/AvgChOffsetsp6H.dat",        iov),
                                            Form("OfflineTimingTool/PhotonClusterEH.dat"		   )};  
    std::vector <std::string> configMedium {Form("OfflineTimingTool/IOV%lu/runByRunFTOffsetsM.dat",     iov),
                                            Form("OfflineTimingTool/IOV%lu/AvgFebOffsetsM.dat",         iov),
                                            Form("OfflineTimingTool/IOV%lu/AvgChOffsetsM.dat",          iov),
                                            Form("OfflineTimingTool/IOV%lu/AvgEnSlFitM.dat",            iov),
                                            Form("OfflineTimingTool/IOV%lu/AvgdPhidEtaSlFitM.dat",      iov),
                                            Form("OfflineTimingTool/IOV%lu/Avgf1f3SlFitM.dat",          iov),
                                            Form("OfflineTimingTool/IOV%lu/AvgChOffsetsp6M.dat",        iov),
                                            Form("OfflineTimingTool/PhotonClusterEM.dat"                   )};

    // Loop over each correction file and load to local vector
    for( size_t nCorr = 0; nCorr < configHigh.size(); nCorr++){

      // high and medium gain files
      std::ifstream f2( PathResolverFindCalibFile( configHigh[nCorr].c_str() ) );
      std::ifstream f3( PathResolverFindCalibFile( configMedium[nCorr].c_str() ) );
      // Make sure opened correctly
      if( !f2.is_open() ){
        Error( "OfflineTimingTool::initialize()", Form("!!! Failed to open: %s",configHigh[nCorr].c_str()));
        return EL::StatusCode::FAILURE;
      }
      if( !f3.is_open() ){
        Error("OfflineTimingTool::initialize()", Form("!!! Failed to open: %s",configMedium[nCorr].c_str() ));
        return EL::StatusCode::FAILURE;
      }
      // to read each line and separate the corrections
      std::string line;
      std::vector< std::string > corrections;
      // High gain corrections
      while( std::getline(f2, line) ){
        boost::split( corrections, line, boost::is_any_of(" "), boost::token_compress_on);
        switch( nCorr ){
          case 0: 
            p0CorrH.at(iov).push_back(corrections); break;
          case 1:
            p1CorrH.at(iov).push_back(corrections[1]); break;
          case 2:
            p2CorrH.at(iov).push_back(corrections[1]); break;
          case 3:
            p3CorrH.at(iov).push_back(corrections); break;
          case 4:
            p4CorrH.at(iov).push_back(corrections); break;
          case 5:
            p5CorrH.at(iov).push_back(corrections); break;
          case 6:
            p6CorrH.at(iov).push_back(corrections[1]); break;
          case 7:
            pPhotonCorrH.at(iov).push_back(corrections); break;
        }
      } // end loading high gain
      f2.close();
      // Medium gain corrections
      while( std::getline(f3, line) ){
        boost::split( corrections, line, boost::is_any_of(" "), boost::token_compress_on);
        switch( nCorr){
          case 0: 
            p0CorrM.at(iov).push_back(corrections); break;
          case 1:
            p1CorrM.at(iov).push_back(corrections[1]); break;
          case 2:
            p2CorrM.at(iov).push_back(corrections[1]); break;
          case 3:
            p3CorrM.at(iov).push_back(corrections); break;
          case 4:
            p4CorrM.at(iov).push_back(corrections); break;
          case 5:
            p5CorrM.at(iov).push_back(corrections); break;
          case 6:
            p6CorrM.at(iov).push_back(corrections[1]); break;
          case 7:
            pPhotonCorrM.at(iov).push_back(corrections); break;
        }
      } // end loading medium gain
      f3.close();

      // Print how many corrections were loaded
      if ( m_debug ){
        switch( nCorr ){
          case 0:
            Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded IOV %lu Pass %lu corrections, %lu High and %lu Med Corrections",
                  iov, nCorr, p0CorrH.at(iov).size(), p0CorrM.at(iov).size()) ); break;
          case 1:
            Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded IOV %lu Pass %lu corrections, %lu High and %lu Med Corrections",
                  iov, nCorr, p1CorrH.at(iov).size(), p1CorrM.at(iov).size()) ); break;
          case 2:
            Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded IOV %lu Pass %lu corrections, %lu High and %lu Med Corrections",
                  iov, nCorr, p2CorrH.at(iov).size(), p2CorrM.at(iov).size()) ); break;
          case 3:
            Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded IOV %lu Pass %lu corrections, %lu High and %lu Med Corrections",
                  iov, nCorr, p3CorrH.at(iov).size(), p3CorrM.at(iov).size()) ); break;
          case 4:
            Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded IOV %lu Pass %lu corrections, %lu High and %lu Med Corrections",
                  iov, nCorr, p4CorrH.at(iov).size(), p4CorrM.at(iov).size()) ); break;
          case 5:
            Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded IOV %lu Pass %lu corrections, %lu High and %lu Med Corrections",
                  iov, nCorr, p5CorrH.at(iov).size(), p5CorrM.at(iov).size()) ); break;
          case 6:
            Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded IOV %lu Pass %lu corrections, %lu High and %lu Med Corrections",
                  iov, nCorr, p6CorrH.at(iov).size(), p6CorrM.at(iov).size()) ); break;
          case 7:
            Info( "OfflineTimingTool::initialize()", Form(" ->Successfully Loaded IOV %lu Additional Photon corrections, %lu High and %lu Med Corrections",
                  iov, pPhotonCorrH.at(iov).size(), pPhotonCorrM.at(iov).size()) ); break;

        } // end printing loaded constants
      }  

    } // end loop over loading configuration files
  } // end of loop of IOVs

  Info( "OfflineTimingTool::initialize()", "    -> Successfully loaded correction configuration files");

  return EL::StatusCode::SUCCESS;
}

//
// Find the IOV given runNumber
//____________________________________________________
EL::StatusCode OfflineTimingTool::setIOV(caloObject_t &calObj){

  unsigned int rN  = calObj.m_rn;
  unsigned int pos = std::find( runNumberList.begin(), runNumberList.end(), rN) - runNumberList.begin();
  
  if(pos == runNumberList.size()){
    Warning( "OfflineTimingTool::initialize()", Form("This run is not in the GRL"));
    return EL::StatusCode::FAILURE;
  }
  
  calObj.m_iov = -1;
  unsigned int cumRuns = 0;
  for(unsigned int i = 1; i <= iovNumberList.size(); i++){
    
    cumRuns += iovNumberList[i - 1];

    if(pos < cumRuns){
      calObj.m_iov = i;
      cumRuns -= iovNumberList[i - 1];
      break;
    }
    
  }

  if (calObj.m_iov == -1){
    Error( "OfflineTimingTool::initialize()", Form("IOV not set properly."));
    return EL::StatusCode::FAILURE;
  }

  if      ( calObj.m_iov < 3  ) calObj.m_year = 2015;
  else if ( calObj.m_iov < 8  ) calObj.m_year = 2016;
  else if ( calObj.m_iov < 11 ) calObj.m_year = 2017;
  else if ( calObj.m_iov < 14 ) calObj.m_year = 2018;
  else if ( calObj.m_iov < 16 ) calObj.m_year = 2022;
  else if ( calObj.m_iov < 20000 ) calObj.m_year = 2023;
  else {
    Error("OfflineTimingTool::initialize()", Form("Invalid IOV. Year cannot be set properly."));
    return EL::StatusCode::FAILURE;
  }

  // Set run number index within the relevant IOV
  if ( pos >= cumRuns && (pos - cumRuns) < runNumberList.size() ) calObj.m_rn_ind = pos - cumRuns;
  else{
    Warning( "OfflineTimingTool::initialize()", Form("Run index not set properly."));
    return EL::StatusCode::FAILURE;
  }
  
  return EL::StatusCode::SUCCESS;
}

//
// Calculate and set dPhi/dEta
//____________________________________________________
EL::StatusCode OfflineTimingTool::setdPhidEta(caloObject_t &calObj){
  
  // Retrieve etas2/phis2
  float etas2 = calObj.m_etas2;
  float phis2 = calObj.m_phis2;
  // Retrieve rectangular cell coordinates
  float cellX = calObj.m_x; 
  float cellY = calObj.m_y;
  float cellZ = calObj.m_z;
  // Calculate spherical cell coordinates
  double cellR     = sqrt( cellX*cellX + cellY*cellY + cellZ*cellZ);
  double cellphi   = TMath::ATan2( cellY, cellX );
  double celltheta = TMath::ACos( cellZ/cellR );
  double celleta   = -TMath::Log( TMath::Tan(celltheta/2) );
  // Calculate dPhi/dEta
  double deta = celleta - etas2; 
  double dphi = cellphi - phis2;
  if( dphi > TMath::Pi() )
    dphi = dphi - 2*TMath::Pi();
  else if ( dphi < -TMath::Pi() )
    dphi = dphi + 2*TMath::Pi();
  // Put dPhi/dEta in correct units
  dphi = dphi/0.0245; // 0.0245 units
  deta = deta/0.025;  // 0.025 units
  // Set the variables in calObj
  calObj.m_dphi = dphi;
  calObj.m_deta = deta;
 
  return EL::StatusCode::SUCCESS;
}


//
// Dump info from caloObject, for debugging
//__________________________________________________
void OfflineTimingTool::dumpInfo(caloObject_t &curr_obj){

    Info("OfflineTimingTool::dumpInfo", Form("\nIsPhoton: %u\nRunNumber: %u\nRunNumber Index: %u\nIOV: %d\nYear: %d\nOnline ID: %lu\nGain: %d\nEnergy: %f\nX: %f\nY: %f\nZ: %f\nClusterE: %f\nEtas2: %f\nPhis2: %f\nf1: %f\nf3: %f\nPV_z: %f\ndphi: %f\ndeta: %f\nValidity: %u\nFEB: %d\nFT: %d\nSlot: %d\nCh: %d\n",
                curr_obj.m_isPhoton,
                curr_obj.m_rn,
                curr_obj.m_rn_ind, 
                curr_obj.m_iov,
		curr_obj.m_year, 
                curr_obj.m_onlId,
                curr_obj.m_gain,
                curr_obj.m_energy,
                curr_obj.m_x,
                curr_obj.m_y,
                curr_obj.m_z,
                curr_obj.m_clusterE,
                curr_obj.m_etas2,
                curr_obj.m_phis2,
                curr_obj.m_f1,
                curr_obj.m_f3,
                curr_obj.m_PV_z,
                curr_obj.m_dphi,
                curr_obj.m_deta,
                (unsigned int)curr_obj.valid, 
                getFebNo(curr_obj.m_onlId),
                getFtNo(curr_obj.m_onlId),
                getSlotNo(curr_obj.m_onlId),
                getChannelNo(curr_obj.m_onlId)) );

}
